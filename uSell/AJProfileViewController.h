//
//  AJProfileViewController.h
//  USell
//
//  Created by Adam Johnson on 8/11/14.
//
//

#import <UIKit/UIKit.h>
#import "AJStatesTableViewController.h"

@interface AJProfileViewController : UIViewController <AJSavedSchoolsProtocalTwo>

@end
