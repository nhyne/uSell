//
//  AJEditViewController.h
//  USell
//
//  Created by Adam Johnson on 9/1/14.
//
//

#import <UIKit/UIKit.h>
#import "AJItemViewController.h"

@interface AJEditViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, AJItemSaleProtocal>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
