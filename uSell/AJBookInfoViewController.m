//
//  AJBookInfoViewController.m
//  U
//
//  Created by Adam Johnson on 7/18/14.
//
//

#import "AJBookInfoViewController.h"
#import "AJChatViewController.h"

@interface AJBookInfoViewController () <UIAlertViewDelegate>


@property (strong, nonatomic) IBOutlet UIImageView *itemSellerImageView;
@property (strong, nonatomic) IBOutlet UIImageView *itemImageView;
@property (strong, nonatomic) IBOutlet UILabel *itemSellerTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemTitleTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemDescriptionTextLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *sellerActivityIndicator;

@property (strong, nonatomic) IBOutlet UILabel *itemCostTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemSellerSchoolLabel;


@end

@implementation AJBookInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self handleColors];
    self.sellerActivityIndicator.color = [AJColors goldColor];
    self.sellerActivityIndicator.hidden = YES;
    [self.sellerActivityIndicator stopAnimating];
    
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        self.sellerActivityIndicator.hidden = NO;
        [self.sellerActivityIndicator startAnimating];
        // connected, do some internet stuff
        
        
        
        PFUser *seller = self.item[@"seller"];
        [seller fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            self.itemSellerTextLabel.text = [NSString stringWithFormat:@"%@ %@",seller[@"first_name"], seller[@"last_name"]];
            PFFile *imageFile = seller[@"picture"];
            [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                if (!error) {
                    UIImage *image = [UIImage imageWithData:data];
                    self.itemSellerImageView.image = image;
                }
                else {
                    NSLog(@"%@", error);
                }
            }];
        }];
        
        PFFile *file = self.item[@"picture"];
        [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *image = [UIImage imageWithData:data];
                self.itemImageView.image = image;
                self.sellerActivityIndicator.hidden = YES;
                [self.sellerActivityIndicator stopAnimating];
            }
            else NSLog(@"%@", error);
        }];
    }
    
    
    self.itemTitleTextLabel.text = self.item[@"title"];
    self.itemDescriptionTextLabel.text = self.item[@"description"];
    self.itemCostTextLabel.text = [NSString stringWithFormat:@"%@", self.item[@"cost"]];
    self.itemSellerSchoolLabel.text = self.item[@"sellerSchool"];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //we want to grab the correct chat room for these users
    if ([segue.identifier isEqualToString:@"bookInfoToChatSegue"] && [segue.destinationViewController isKindOfClass:[AJChatViewController class]]) {
        
        
        
        
        AJChatViewController *nextView = segue.destinationViewController;
        
        PFQuery *query = [PFQuery queryWithClassName:@"ChatRoom"];
        [query whereKey:@"user1" equalTo:[PFUser currentUser]];
        [query whereKey:@"user2" equalTo:self.item[@"seller"]];
        PFQuery *queryInverse = [PFQuery queryWithClassName:@"ChatRoom"];
        [queryInverse whereKey:@"user2" equalTo:[PFUser currentUser]];
        [queryInverse whereKey:@"user1" equalTo:self.item[@"seller"]];
        PFQuery *queryCombined = [PFQuery orQueryWithSubqueries:@[query, queryInverse]];
        [queryCombined includeKey:@"user1"];
        [queryCombined includeKey:@"user2"];
        
        
        PFObject *testChatRoom = [queryCombined getFirstObject];
        
        if (testChatRoom != nil) {
            nextView.chatRoom = testChatRoom;
        }
        else {
            nextView.chatRoom = [PFObject objectWithClassName:@"ChatRoom" dictionary:@{@"user1" : [PFUser currentUser], @"user2" : self.item[@"seller"]}];
            [nextView.chatRoom save];
        }
    
    }
    
    
}

#pragma mark - Actions

- (IBAction)chatBarButtonItemPressed:(UIBarButtonItem *)sender {
    
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // connected, do some internet stuff
        [self performSegueWithIdentifier:@"bookInfoToChatSegue" sender:sender];
    }
}


- (IBAction)itemPictureButtonPressed:(UIButton *)sender {
    
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // connected, do some internet stuff
        [self handleItemStarring:[self getStarredString]];
        
    }
    
}


#pragma mark - UIAlertView Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        NSString *starredString = [self getStarredString];
        NSMutableArray *newSavedArray = [[PFUser currentUser][starredString] mutableCopy];
        if (!newSavedArray) {
            newSavedArray = [[NSMutableArray alloc]init];
        }
        NSLog(@"old array: %@", newSavedArray);
        NSLog(@"%@", [self.item objectId]);
        [newSavedArray addObject:[self.item objectId]];
        NSLog(@"array: %@", newSavedArray);
        NSLog(@"key : %@", starredString);
        [[PFUser currentUser]setObject:newSavedArray forKey:starredString];
        [[PFUser currentUser] saveInBackground];
    }
    
}

#pragma mark - Helper Methods

-(void) handleItemStarring :(NSString *) starredString {
    
    if (![[PFUser currentUser][starredString] containsObject:[self.item objectId]]) { //does not have item saved yet
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Star Item?" message:@"Do you want to save this item to your starred items?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Star", nil];
        [alert show];
        
        
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Already Starred" message:@"You have already starred this item" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles: nil];
        [alert show];
    }
    
    
}

-(NSString *) getStarredString {
    NSString *itemClassName = [self.item parseClassName];
    
    if ([itemClassName isEqualToString:@"Book"]) {
        return @"starredBooks";
    }
    else if ([itemClassName isEqualToString:@"Electronic"]) {
        return @"starredElectronics";
    }
    else if ([itemClassName isEqualToString:@"Room"]) {
        return @"starredRooms";
    }
    else if ([itemClassName isEqualToString:@"Other"]) {
        return @"starredOthers";
    }
    else {
        NSLog(@"error due to getting starred string");
        return @"error";
    }
}

-(void) handleColors {
    
    self.view.backgroundColor = [AJColors backgroundColor];
    self.itemCostTextLabel.textColor = [AJColors goldColor];
    self.itemSellerSchoolLabel.textColor = [AJColors goldColor];
    self.itemSellerTextLabel.textColor = [AJColors goldColor];
    self.itemTitleTextLabel.textColor = [AJColors goldColor];
    self.itemDescriptionTextLabel.textColor = [AJColors goldColor];    
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

@end
