//
//  AJStatesTableViewController.h
//  USell
//
//  Created by Adam Johnson on 8/15/14.
//
//

#import <UIKit/UIKit.h>
#import "AJSchoolsTableViewController.h"
@protocol AJSavedSchoolsProtocalTwo <NSObject>

-(void) updateSavedSchools :(NSArray *) newSavedSchoolsArray;

@end

@interface AJStatesTableViewController : UITableViewController  <AJSavedSchoolsProtocalOne>

@property (weak, nonatomic) id <AJSavedSchoolsProtocalTwo> delegate;
@property (strong, nonatomic) NSMutableArray *savedSchoolsArray;
@end
