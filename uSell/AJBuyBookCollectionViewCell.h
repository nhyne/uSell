//
//  AJBuyBookCollectionViewCell.h
//  U
//
//  Created by Adam Johnson on 7/19/14.
//
//

#import <UIKit/UIKit.h>

@interface AJBuyBookCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *itemTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *itemActivityIndacator;


@end
