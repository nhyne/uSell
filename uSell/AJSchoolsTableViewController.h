//
//  AJSchoolsTableViewController.h
//  USell
//
//  Created by Adam Johnson on 8/15/14.
//
//

#import <UIKit/UIKit.h>

@protocol AJSavedSchoolsProtocalOne <NSObject>

-(void)updateSavedSchools :(NSArray *) newSavedSchoolsArray;

@end


@interface AJSchoolsTableViewController : UITableViewController
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *stateAbbreviation;
@property (weak, nonatomic) id <AJSavedSchoolsProtocalOne> delegate;
@property (strong, nonatomic) NSMutableArray *savedSchoolsArray;
@end
