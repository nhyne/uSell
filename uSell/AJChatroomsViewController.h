//
//  AJChatroomsViewController.h
//  USell
//
//  Created by Adam Johnson on 9/6/14.
//
//

#import <UIKit/UIKit.h>

@interface AJChatroomsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
