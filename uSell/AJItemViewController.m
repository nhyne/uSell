//
//  AJItemViewController.m
//  USell
//
//  Created by Adam Johnson on 8/10/14.
//
//

#import "AJItemViewController.h"

@interface AJItemViewController () <UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *itemImageView;
@property (strong, nonatomic) IBOutlet UIButton *itemImageUploadedButtonPressed;
@property (strong, nonatomic) IBOutlet UITextField *itemTitleTextField;
@property (strong, nonatomic) IBOutlet UITextView *itemDescriptionTextView;
@property (strong, nonatomic) IBOutlet UITextField *itemCostTextField;
@property (strong, nonatomic) IBOutlet UIPickerView *itemTypePicker;
@property (strong, nonatomic) NSArray *pickerData;
@property (strong, nonatomic) NSString *pickerSelection;

@property (strong, nonatomic) IBOutlet UILabel *itemTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemDescriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemCostLabel;


@property (strong, nonatomic) IBOutlet UIButton *itemImageUploadedButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelItemSaleButton;
@property (strong, nonatomic) IBOutlet UIButton *itemSaleCreatedButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic) BOOL imageUploaded;
@property (nonatomic) BOOL classTypeUpdated;

@end

@implementation AJItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self handleColors];
    self.itemTitleTextField.delegate = self;
    self.itemDescriptionTextView.delegate = self;
    self.itemCostTextField.delegate = self;
    self.itemTypePicker.delegate = self;
    self.itemTypePicker.dataSource = self;
    self.imageUploaded = NO;
    self.classTypeUpdated = NO;
    self.pickerData = @[@"Book", @"Electronic", @"Room", @"Other"];
    
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // connected, do some internet stuff
        
        if (self.item != nil) {
            [self.itemTypePicker selectRow:[self findPickerRow] inComponent:0 animated:NO];
            self.pickerSelection = self.pickerData[[self findPickerRow]];
            self.itemTitleTextField.text = self.item[@"title"];
            self.itemDescriptionTextView.text = self.item[@"description"];
            self.itemCostTextField.text = [NSString stringWithFormat:@"%@", self.item[@"cost"]];
            [self.itemSaleCreatedButton setTitle:@"Update Sale" forState:UIControlStateNormal];
            [self.cancelItemSaleButton setTitle:@"Cancel Update" forState:UIControlStateNormal];
            [self.itemImageUploadedButton setTitle:@"Edit" forState:UIControlStateNormal];
            PFFile *itemPicture = self.item[@"picture"];
            [itemPicture getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                if (!error) {
                    UIImage *image = [UIImage imageWithData:data];
                    self.itemImageView.image = image;
                }
                else NSLog(@"%@", error);
            }];
        }
        
        else {
            [self.itemSaleCreatedButton setTitle:@"Create Sale" forState:UIControlStateNormal];
            [self.cancelItemSaleButton setTitle:@"Cancel Sale" forState:UIControlStateNormal];
            [self.itemImageUploadedButton setTitle:@"Upload Image" forState:UIControlStateNormal];
            self.pickerSelection = @"Book";
        }
        
    }
    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)itemImageUploadedButtonPressed:(UIButton *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
        
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    
    [self presentViewController:picker animated:YES completion:nil];
    self.imageUploaded = YES;
    
}


- (IBAction)cancelItemSaleButtonPressed:(UIButton *)sender {
    if (self.item != nil) {
        [self.delegate updateView:self.item :self.pickerSelection];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)itemSaleCreatedButtonPressed:(UIButton *)sender {
    
    if (self.itemImageView != nil && ![self.itemDescriptionTextView.text isEqualToString:@"Description"] && ![self.itemCostTextField.text isEqualToString:@""] && ![self.itemTitleTextField.text isEqualToString:@""]) {
        
        [self.itemSaleCreatedButton setUserInteractionEnabled:NO];
        [self dismissViewControllerAnimated:YES completion:^{
            if (self.item != nil) {
                PFQuery *queryForItem = [PFQuery queryWithClassName:[self.item parseClassName]];
                [queryForItem getObjectInBackgroundWithId:[self.item objectId] block:^(PFObject *object, NSError *error) {
                    if (!error) {
                        if (self.classTypeUpdated) {
                            [self updateItemClass:self.item :self.pickerSelection];
                        }
                        else {
                            [object setValue:self.itemTitleTextField.text forKey:@"title"];
                            [object setValue:self.itemDescriptionTextView.text forKey:@"description"];
                            [object setValue:@([self.itemCostTextField.text intValue])  forKeyPath:@"cost"];
                            if (self.imageUploaded) {
                                NSData *imageData = UIImageJPEGRepresentation(self.itemImageView.image, 0.8);
                                if (!imageData) {
                                    NSLog(@"Image data was not found");
                                }
                                PFFile *photoFile = [PFFile fileWithData:imageData];
                                [object setObject:photoFile forKey:kAJBookPictureKey];
                            }
                            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                
                                [self.delegate updateView:object :self.pickerSelection];
                                self.item = nil;
                                //[self dismissViewControllerAnimated:NO completion:nil];
                            }];
                        }
                    }
                    else {
                        NSLog(@"%@", error);
                    }
                }];
            }
            else {
                PFObject *newObject = [PFObject objectWithClassName:self.pickerSelection dictionary:@{@"title" : self.itemTitleTextField.text, @"description" : self.itemDescriptionTextView.text, @"cost" : @([self.itemCostTextField.text intValue]), @"seller" : [PFUser currentUser], @"visible" : @1, @"sellerSchool" : [NSString stringWithFormat:@"%@, %@", [PFUser currentUser][@"school"], [PFUser currentUser][@"state"]]}];
                if (self.imageUploaded) {
                    NSData *imageData = UIImageJPEGRepresentation(self.itemImageView.image, 0.8);
                    if (!imageData) {
                        NSLog(@"Image data was not found");
                    }
                    PFFile *photoFile = [PFFile fileWithData:imageData];
                    [newObject setObject:photoFile forKey:@"picture"];
                }
                [newObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    [self.delegate updateView:newObject :self.pickerSelection];
                    self.item = nil;
                    //[self dismissViewControllerAnimated:YES completion:nil];
                }];
            }
        }];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Item Incomplete" message:@"You forgot to fill in some information!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles: nil];
        [alert show];
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
    
}

#pragma mark - UIImagePickerControllerDelegate


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) {
        image = info[UIImagePickerControllerOriginalImage];
        
    }
    self.itemImageView.image = image;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - TextView Methods

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
    
}

#pragma mark - pickerView Methods

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.pickerData count];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.pickerData[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.pickerSelection = self.pickerData[row];
    self.classTypeUpdated = YES;
    
}

-(NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSAttributedString *attString = [[NSAttributedString alloc]initWithString:self.pickerData[row] attributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:220/255.0f green:175/255.0f blue:90/255.0f alpha:1.0f]}];
    return attString;
                                     
}

#pragma mark - UIAlertViewDelegate Methods


#pragma mark - Helper Methods

-(void)keyboardWasShown :(NSNotification*)notification {
    
    NSDictionary *dictionary = [notification userInfo];
    CGSize keyboardSize = [[dictionary objectForKey:UIKeyboardFrameBeginUserInfoKey]CGRectValue].size;
    [self.scrollView setContentOffset:CGPointMake(0, keyboardSize.height) animated:YES];
    
}

-(IBAction)textFieldDidBeginEditing:(UITextField *)sender {
    sender.delegate = self;
}

-(void)keyboardWillBeHidden :(NSNotification*)notification {
    
    [self.scrollView setContentOffset:CGPointMake(0, 0)animated:YES];
    
}

-(NSInteger) findPickerRow {
    
    NSString *tempString = [self.item parseClassName];
    if ([tempString isEqualToString:@"Book"]) {
        return 0;
    }
    else if ([tempString isEqualToString:@"Electronic"]) {
        return 1;
    }
    else if ([tempString isEqualToString:@"Room"]) {
        return 2;
    }
    else{// ([tempString isEqualToString:@"Other"]) {
        return 3;
    }
}

-(void) updateItemClass :(PFObject *)object :(NSString *)newClass {
    
    PFQuery *queryForObject = [PFQuery queryWithClassName:[self.item parseClassName]];
    [queryForObject getObjectInBackgroundWithId:[object objectId] block:^(PFObject *object, NSError *error) {
        if (!error) {
            [object setValue:@0 forKeyPath:@"visible"];
            [object saveInBackground];
            PFObject *newObject = [PFObject objectWithClassName:newClass dictionary:@{@"title" : self.itemTitleTextField.text, @"description" : self.itemDescriptionTextView.text, @"cost" : @([self.itemCostTextField.text intValue]), @"seller" : [PFUser currentUser], @"visible" : @1, @"sellerSchool" : [PFUser currentUser][@"school"]}];
            NSData *imageData = UIImageJPEGRepresentation(self.itemImageView.image, 0.8);
            if (!imageData) {
                NSLog(@"Image data was not found");
            }
            PFFile *photoFile = [PFFile fileWithData:imageData];
            [newObject setObject:photoFile forKey:@"picture"];
            [newObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                [self dismissViewControllerAnimated:YES completion:nil];
                [self.delegate updateView:newObject :self.pickerSelection];
                self.item = nil;
            }];
        }
        else {
            NSLog(@"%@", error);
        }
    }];
    
    
}

-(void) handleColors {
    
    self.view.backgroundColor = [AJColors backgroundColor];
    self.itemCostLabel.textColor = [AJColors goldColor];
    self.itemDescriptionLabel.textColor = [AJColors goldColor];
    self.itemTitleLabel.textColor = [AJColors goldColor];
    self.itemCostTextField.textColor = [AJColors goldColor];
    self.itemDescriptionTextView.textColor = [AJColors goldColor];
    self.itemTitleTextField.textColor = [AJColors goldColor];
    self.itemCostTextField.backgroundColor = [AJColors garnetColor];
    self.itemDescriptionTextView.backgroundColor = [AJColors garnetColor];
    self.itemTitleTextField.backgroundColor = [AJColors garnetColor];
    [self.itemImageUploadedButton setTitleColor:[AJColors goldColor] forState:UIControlStateNormal];
    
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}



@end
