//
//  AJMainCollectionViewCell.h
//  U
//
//  Created by Adam Johnson on 8/6/14.
//
//

#import <UIKit/UIKit.h>

@interface AJMainCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellImageView;
@property (weak, nonatomic) IBOutlet UILabel *cellTextLabel;


@end
