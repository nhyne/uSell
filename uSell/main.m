//
//  main.m
//  uSell
//
//  Created by Adam Johnson on 1/2/15.
//  Copyright (c) 2015 Adam Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
