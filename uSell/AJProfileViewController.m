//
//  AJProfileViewController.m
//  USell
//
//  Created by Adam Johnson on 8/11/14.
//
//

#import "AJProfileViewController.h"
#import "AJStateAndSchoolData.h"

@interface AJProfileViewController () <UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *userProfilePictureImageView;
@property (strong, nonatomic) NSArray *colleges;
@property (strong, nonatomic) NSArray *stateAbbreviations;
@property (strong, nonatomic) IBOutlet UIPickerView *stateCollegePickerView;
@property (strong, nonatomic) IBOutlet UILabel *schoolNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *yourSchoolLabel;
@property (strong, nonatomic) IBOutlet UILabel *viewingSchoolsLabel;
@property (strong, nonatomic) IBOutlet UITableView *savedSchoolsTableView;
@property (strong, nonatomic) NSMutableArray *savedSchoolsArray;
@property (strong, nonatomic) IBOutlet UIButton *starsButton;

@property (weak, nonatomic) IBOutlet UIButton *addSchoolsButton;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *backBarButtonItem;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editBarButtonItem;

@property (strong, nonatomic) NSString *pickerStateSelection;
@property (strong, nonatomic) NSString *pickerCollegeSelection;
@property (nonatomic) NSUInteger pickerStateIndex;
@property (nonatomic) NSUInteger pickerCollegeIndex;

@property (nonatomic) BOOL editModeEnabled;
@property (nonatomic) BOOL imageUploaded;
@property (nonatomic) BOOL didEdit;
@property (nonatomic) BOOL updatedSchools;


@end

@implementation AJProfileViewController

-(NSArray *) colleges {
    
    if (!_colleges) {
        _colleges = [[NSArray alloc]init];
    }
    return _colleges;
}
-(NSArray *) stateAbbreviations {
    if (!_stateAbbreviations) {
        _stateAbbreviations = [[NSArray alloc]init];
    }
    return _stateAbbreviations;
}
- (NSMutableArray *) savedSchoolsArray {
    if (!_savedSchoolsArray) {
        _savedSchoolsArray = [[NSMutableArray alloc]init];
    }
    return _savedSchoolsArray;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // connected, do some internet stuff
        PFFile *file = [PFUser currentUser][@"picture"];
        [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *image = [UIImage imageWithData:data];
                self.userProfilePictureImageView.image = image;
            }
            else NSLog(@"%@", error);
        }];
        
    }
    
    
    [self buildColleges];
    [self disableEditing];
    [self handleColors];
    self.savedSchoolsTableView.delegate = self;
    self.savedSchoolsTableView.dataSource = self;
    self.didEdit = NO;
    self.imageUploaded = NO;
    self.updatedSchools = NO;
    self.stateCollegePickerView.delegate = self;
    self.stateCollegePickerView.dataSource = self;
    self.schoolNameLabel.text = [PFUser currentUser][@"school"];
    self.savedSchoolsArray = [PFUser currentUser][@"savedSchools"];
    
    
    
    // Do any additional setup after loading the view.
}

-(void) viewWillDisappear:(BOOL)animated {
    
    if (self.didEdit) {
        [self saveEdits];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.destinationViewController isKindOfClass:[AJStatesTableViewController class]] && [segue.identifier isEqualToString:@"profileToStatesSegue"]) {
        AJStatesTableViewController *nextView = segue.destinationViewController;
        nextView.savedSchoolsArray = self.savedSchoolsArray;
        nextView.delegate = self;
    }
    
}



#pragma mark - pickerView Methods

-(NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *tempString;
    if (component == 0) {
        tempString = self.stateAbbreviations[row];
    }
    else if (component == 1) {
        tempString = [self.colleges objectAtIndex:self.pickerStateIndex][row];
    }
    else {
        tempString = @"error";
    }
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:tempString attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:220/255.0f green:175/255.0f blue:90/255.0f alpha:1.0f]}];
    return attString;
    
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (component == 0) {
        return [self.colleges count];
    }
    else if (component == 1) {
        NSArray *currentStateColleges = [self.colleges objectAtIndex:self.pickerStateIndex];
        return [currentStateColleges count];
        //return [[self.colleges objectAtIndex:*(self.pickerStateIndex)] count];
    }
    else {
        return 0;
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (component == 0) {
        //self.pickerStateIndex = row;
        return self.stateAbbreviations[row];
    }
    else if (component == 1) {
        //self.pickerCollegeIndex = row;
        return [self.colleges objectAtIndex:self.pickerStateIndex][row];
    }
    else {
        return @"error";
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (component == 0) {
        self.pickerStateSelection = self.colleges[row];
        self.pickerStateIndex = row;
        [pickerView reloadComponent:1];
    }
    else if (component == 1) {
        self.pickerCollegeSelection = [self.colleges objectAtIndex:self.pickerStateIndex][row];
        self.pickerCollegeIndex = row;
    }
    
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 0) {
        return 50.0f;
    }
    else if (component == 1){
        return 265.0f;
    }
    else {
        return 0;
    }
}

#pragma mark - Actions

- (IBAction)editBarButtonItemPressed:(UIBarButtonItem *)sender {
    
    if (!self.editModeEnabled) { //enter edit mode
        self.editModeEnabled = YES;
        self.schoolNameLabel.hidden = YES;
        self.navigationItem.rightBarButtonItem.title = @"Done";
        [self.stateCollegePickerView setUserInteractionEnabled:YES];
        self.stateCollegePickerView.hidden = NO;
        [self.stateCollegePickerView setAlpha:0.6f];
        [self.userProfilePictureImageView setAlpha:0.6f];
        self.navigationItem.leftBarButtonItem.title = @"Cancel";
        self.yourSchoolLabel.hidden = YES;
        self.addSchoolsButton.hidden = NO;
        [self.addSchoolsButton setUserInteractionEnabled:YES];
        self.savedSchoolsTableView.hidden = YES;
        [self.savedSchoolsTableView setUserInteractionEnabled:NO];
        self.viewingSchoolsLabel.hidden = YES;
        self.starsButton.hidden = YES;
        [self.starsButton setUserInteractionEnabled:NO];
    }
    
    else { //not editing
        self.didEdit = YES;
        [self disableEditing];
        //[self saveEdits];
        
        if (self.pickerCollegeIndex > [self.colleges[self.pickerStateIndex] count]) {
            int position = [self.colleges[self.pickerStateIndex] count] - 1;
            self.schoolNameLabel.text = self.colleges[self.pickerStateIndex][position];
        }
        else {
            self.schoolNameLabel.text = self.colleges[self.pickerStateIndex][self.pickerCollegeIndex];
        }
        
        //will also need to save new data to parse
    }
    
    
}

- (IBAction)backBarButtonItemPressed:(UIBarButtonItem *)sender {
    
    if (!self.editModeEnabled) { //back button
        [self.navigationController popViewControllerAnimated:YES];
    }
    else { //cancel button
        [self disableEditing];
    }
    
    
}

- (IBAction)addSchoolsButtonPressed:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"profileToStatesSegue" sender:sender];
    
}

- (IBAction)starsButtonPressed:(UIButton *)sender {
    [self performSegueWithIdentifier:@"profileToStarredItemsSegue" sender:sender];
}


#pragma mark - UIImagePickerControllerDelegate Methods



-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) {
        image = info[UIImagePickerControllerOriginalImage];
        
    }
    self.userProfilePictureImageView.image = image;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark - tableView Methods

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.backgroundColor = [AJColors garnetColor];
    cell.textLabel.textColor = [AJColors goldColor];
    cell.textLabel.text = self.savedSchoolsArray[indexPath.row];
    return cell;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.savedSchoolsArray count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.savedSchoolsArray removeObjectAtIndex:indexPath.row];
        [self.savedSchoolsTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        self.didEdit = YES;
    }
    
}



#pragma mark - AJSavedSchoolsProtocalTwo Methods

-(void) updateSavedSchools:(NSArray *)newSavedSchoolsArray {
    self.savedSchoolsArray = [newSavedSchoolsArray mutableCopy];
    [self.savedSchoolsTableView reloadData];
    self.updatedSchools = YES;
    
}

#pragma mark - Helper Methods

-(void) buildColleges {
    
    self.colleges = [AJStateAndSchoolData schoolsFromStateAbbreviations];
    self.stateAbbreviations = [AJStateAndSchoolData stateAbbreviations];
    
}

-(void) disableEditing { //exit edit mode
    self.editModeEnabled = NO;
    self.navigationItem.rightBarButtonItem.title = @"Edit";
    [self.stateCollegePickerView setUserInteractionEnabled:NO];
    self.stateCollegePickerView.hidden = YES;
    [self.userProfilePictureImageView setAlpha:1.0f];
    self.schoolNameLabel.hidden = NO;
    self.yourSchoolLabel.hidden = NO;
    self.navigationItem.leftBarButtonItem.title = @"Back";
    self.addSchoolsButton.hidden = YES;
    [self.addSchoolsButton setUserInteractionEnabled:NO];
    self.savedSchoolsTableView.hidden = NO;
    [self.savedSchoolsTableView setUserInteractionEnabled:YES];
    self.viewingSchoolsLabel.hidden = NO;
    self.starsButton.hidden = NO;
    [self.starsButton setUserInteractionEnabled:YES];
}


-(void) saveEdits { //save new data to parse
    //if (self.pickerStateIndex != nil) {
    [[PFUser currentUser] setObject:self.stateAbbreviations[self.pickerStateIndex] forKey:@"state"];
    //}
    if (self.pickerCollegeSelection != nil) {
        [[PFUser currentUser]setObject:self.pickerCollegeSelection forKey:@"school"];
    }
    if (self.updatedSchools) {
        [[PFUser currentUser]setObject:self.savedSchoolsArray forKey:@"savedSchools"];
    }
    if (self.imageUploaded) {
        NSData *imageData = UIImageJPEGRepresentation(self.userProfilePictureImageView.image, 0.8);
        if (!imageData) {
            NSLog(@"Image data was not found");
        }
        PFFile *photoFile = [PFFile fileWithData:imageData];
        [[PFUser currentUser] setObject:photoFile forKey:@"picture"];
    }
    [[PFUser currentUser]saveInBackground];
}


-(void) handleColors {
    
    self.view.backgroundColor = [AJColors backgroundColor];
    self.schoolNameLabel.textColor = [AJColors goldColor];
    self.yourSchoolLabel.textColor = [AJColors goldColor];
    self.savedSchoolsTableView.backgroundColor = [AJColors backgroundColor];
    
    [self.savedSchoolsTableView setSeparatorColor:[AJColors goldColor]];
    [self.addSchoolsButton setTitleColor:[AJColors goldColor] forState:UIControlStateNormal];
    self.viewingSchoolsLabel.textColor = [AJColors goldColor];
    [self.starsButton setTitleColor:[AJColors goldColor] forState:UIControlStateNormal];
    
    
}



- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}







@end
