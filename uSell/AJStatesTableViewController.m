//
//  AJStatesTableViewController.m
//  USell
//
//  Created by Adam Johnson on 8/15/14.
//
//

#import "AJStatesTableViewController.h"
#import "AJStateAndSchoolData.h"

@interface AJStatesTableViewController ()

@property (strong, nonatomic) NSArray *states;
@property (strong, nonatomic) NSDictionary *stateAbbreviations;
@property (strong, nonatomic) NSString *tempState;

@end

@implementation AJStatesTableViewController

-(NSMutableArray *) savedSchoolsArray {
    if (!_savedSchoolsArray) {
        _savedSchoolsArray = [[NSMutableArray alloc]init];
    }
    return _savedSchoolsArray;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self buildStates];
    self.tableView.backgroundColor = [AJColors backgroundColor];
    [self.tableView setSeparatorColor:[AJColors goldColor]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void) viewWillDisappear:(BOOL)animated {
    [self.delegate updateSavedSchools:self.savedSchoolsArray];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 50;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.backgroundColor = [AJColors garnetColor];
    cell.textLabel.textColor = [AJColors goldColor];
    cell.textLabel.text = self.states[indexPath.row];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.tempState = self.states[indexPath.row];
    [self performSegueWithIdentifier:@"statesToSchoolsSegue" sender:nil];
    
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"statesToSchoolsSegue"] && [segue.destinationViewController isKindOfClass:[AJSchoolsTableViewController class]]) {
        AJSchoolsTableViewController *nextView = segue.destinationViewController;
        nextView.savedSchoolsArray = self.savedSchoolsArray;
        nextView.state = self.tempState;
        nextView.stateAbbreviation =  self.stateAbbreviations[self.tempState];
        self.tempState = nil;
        nextView.delegate = self;
    }
}

#pragma mark - AJSavedSchoolsProtocalOne Methods

-(void) updateSavedSchools:(NSArray *)newSavedSchoolsArray {
    
    self.savedSchoolsArray = [newSavedSchoolsArray mutableCopy];
    
}



#pragma mark - Helper Methods

-(void) buildStates {
    self.states = [AJStateAndSchoolData stateNames];
    self.stateAbbreviations = [AJStateAndSchoolData stateNamesToAbbreviations];
}



@end
