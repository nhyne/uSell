//
//  AJColors.h
//  USell
//
//  Created by Adam Johnson on 9/1/14.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AJColors : NSObject
+(UIColor *) backgroundColor;
+(UIColor *) garnetColor;
+(UIColor *) goldColor;
@end
