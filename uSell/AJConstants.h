//
//  AJConstants.h
//  U
//
//  Created by Adam Johnson on 7/10/14.
//
//

#import <Foundation/Foundation.h>

@interface AJConstants : NSObject

#pragma mark - Book Class

extern NSString *const kAJBookClassKey;
extern NSString *const kAJBookSellerKey;
extern NSString *const kAJBookVisibleKey;
extern NSString *const kAJBookTitleKey;
extern NSString *const kAJBookAuthorKey;
extern NSString *const kAJBookPictureKey;
extern NSString *const kAJBookConditionKey;
extern NSString *const kAJBookCostKey;
extern NSString *const kAJBookCourseKey;
extern NSString *const kAJBookEditionKey;

#pragma mark - Item Class Numbers

extern int *const kAJBookClassNumberKey;
extern int *const kAJElectronicClassNumberKey;
extern int *const kAJRoomClassNumberKey;
extern int *const kAJOtherClassNumberKey;

#pragma mark - User Class

extern NSString *const kAJUserClassKey;

extern NSString *const kAJUserProfileKey;
extern NSString *const kAJUserProfileNameKey;
extern NSString *const kAJUserProfileFirstNameKey;
extern NSString *const kAJUserProfileLocationKey;
extern NSString *const kAJUserProfileGenderKey;
extern NSString *const kAJUserProfileBirthdayKey;
extern NSString *const kAJUserProfileInterestedInKey;
extern NSString *const kAJUserProfilePictureURL;
extern NSString *const kAJUserProfileRelationshipStatusKey;
extern NSString *const kAJUserProfileAgeKey;


@end
