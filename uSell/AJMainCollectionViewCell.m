//
//  AJMainCollectionViewCell.m
//  U
//
//  Created by Adam Johnson on 8/6/14.
//
//

#import "AJMainCollectionViewCell.h"


@implementation AJMainCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self setup];
    }
    return self;
    
}

-(void) setup {
    
    [self.contentView addSubview:self.cellTextLabel];
    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
