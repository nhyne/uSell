//
//  AJChatViewController.m
//  U
//
//  Created by Adam Johnson on 7/24/14.
//
//

#import "AJChatViewController.h"

@interface AJChatViewController () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) NSMutableArray *chats;

@property (strong, nonatomic) PFUser *withUser;
@property (strong, nonatomic) PFUser *currentUser;

@property (strong, nonatomic) NSTimer *chatsTimer;
@property (nonatomic) BOOL initialLoadComplete;

@property (strong, nonatomic) UIImageView *outgoingBubbleImageView;
@property (strong, nonatomic) UIImageView *incomingBubbleImageView;

@end

@implementation AJChatViewController


-(NSMutableArray *)chats {
    
    if (!_chats) {
        _chats = [[NSMutableArray alloc]init];
    }
    return _chats;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.currentUser = [PFUser currentUser];

    PFUser *testUser1 = self.chatRoom[@"user1"];
    
    [testUser1 fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if ([testUser1.objectId isEqualToString:self.currentUser.objectId]) {
            self.withUser = self.chatRoom[@"user2"];
        }
        else {
            self.withUser = self.chatRoom[@"user1"];
            
        }
        
        self.title = self.withUser[@"first_name"];
        // Do any additional setup after loading the view.
        self.senderId = [PFUser currentUser].objectId;
        self.senderDisplayName = [PFUser currentUser][@"first_name"];
    }];
    
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    //self.collectionView.collectionViewLayout.messageBubbleFont = [UIFont fontWithName:@"Helvetica" size:14.0f];
    //self.incomingBubbleImageView = [JSQMessagesBubbleImageFactory incomingMessageBubbleImageViewWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    //self.outgoingBubbleImageView = [JSQMessagesBubbleImageFactory outgoingMessageBubbleImageViewWithColor:[UIColor jsq_messageBubbleBlueColor]];
    self.showLoadEarlierMessagesHeader = NO;
    self.initialLoadComplete = NO;
    [self checkForNewChats];
    self.chatsTimer = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(checkForNewChats) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidDisappear:(BOOL)animated {
    [self.chatsTimer invalidate];
    self.chatsTimer = nil;
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    /**
     *  Enable/disable springy bubbles, default is NO.
     *  You must set this from `viewDidAppear:`
     *  Note: this feature is mostly stable, but still experimental
     */
    self.collectionView.collectionViewLayout.springinessEnabled = NO;
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.collectionView.backgroundColor = [AJColors backgroundColor];
    
    
}

#pragma mark - JSQMessages Methods

-(void) didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date {
    
    if (text.length != 0) {
        
        PFObject *chat = [PFObject objectWithClassName:@"Chat"];
        [chat setObject:self.chatRoom forKey:@"chatRoom"];
        [chat setObject:date forKey:@"date"];
        [chat setObject:senderId forKey:@"senderId"];
        [chat setObject:senderDisplayName forKey:@"senderDisplayName"];
        [chat setObject:self.currentUser forKey:@"fromUser"];
        [chat setObject:self.withUser forKey:@"toUser"];
        [chat setObject:text forKey:@"text"];
        [chat saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [self.chats addObject:chat];
                [JSQSystemSoundPlayer jsq_playMessageSentSound];
                [self finishSendingMessage];
                [self.collectionView reloadData];
                [self scrollToBottomAnimated:YES];
            }
            else {
                NSLog(@"%@", error);
            }
        }];
    }
    
}


- (void)didPressAccessoryButton:(UIButton *)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Media messages"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Send photo", @"Send location", nil];
    
    [sheet showFromToolbar:self.inputToolbar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    switch (buttonIndex) {
        case 0:
           // [self.demoData addPhotoMediaMessage];
            
//            UIImagePickerController *picker = [[UIImagePickerController alloc]init];
//            picker.delegate = self;
//            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//            }
//            else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
//                
//                picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
//            }
//            
//            [self presentViewController:picker animated:YES completion:nil];
            
            break;
            
        case 1:
        {
            //__weak UICollectionView *weakView = self.collectionView;
            
            //[self.demoData addLocationMediaMessageCompletion:^{
            //    [weakView reloadData];
           // }];
        }
            break;
            
    }
    
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    [self finishSendingMessage];
}



-(id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PFObject *chat = self.chats[indexPath.item];
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:chat[@"senderId"] senderDisplayName:chat[@"senderDisplayName"] date:(NSDate *)chat[@"date"] text:chat[@"text"]];
    //JSQMessage *message = [[JSQMessage alloc]initWithText:chat[@"text"] sender:sender.objectId date:(NSDate *)chat[@"date"]];
    return message;
}

-(id <JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    PFObject *message = [self.chats objectAtIndex:indexPath.item];
//    PFUser *testUser = message[@"fromUser"];
//    
//    if ([testUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
//        return [[JSQMessagesBubbleImage alloc]initWithMessageBubbleImage:[JSQMessagesBubbleImageFactory ] highlightedImage:<#(UIImage *)#>]
//    }
//    
//    return self.chats.incomingBubbleImageData;

    return nil;
}


-(NSString *)sender {
    return [PFUser currentUser].objectId;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
    
}


-(UIImageView *)collectionView:(JSQMessagesCollectionView *)collectionView bubbleImageViewForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PFObject *chat = self.chats[indexPath.item];
    PFUser *testUser = chat[@"fromUser"];
    
    //NSLog(@"%@ %@", self.currentUser.objectId, testUser.objectId);
    if ([self.currentUser.objectId isEqualToString:testUser.objectId]) {
        //sent from this user
        return [[UIImageView alloc] initWithImage:self.outgoingBubbleImageView.image highlightedImage:self.outgoingBubbleImageView.highlightedImage];
    }
    else {
        return [[UIImageView alloc]initWithImage:self.incomingBubbleImageView.image highlightedImage:self.incomingBubbleImageView.highlightedImage];

    }
}

-(UIImageView *)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageViewForItemAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

-(NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.item % 3 == 0) {
        
        PFObject *chat = self.chats[indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:(NSDate *)chat[@"date"]];
    }
    
    return nil;
    
}

-(NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    PFObject *chat = self.chats[indexPath.item];
    //PFUser *sender = chat[@"fromUser"];
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:chat[@"senderId"] senderDisplayName:chat[@"senderDisplayName"] date:(NSDate *)chat[@"date"] text:chat[@"text"]];
    //JSQMessage *message = [[JSQMessage alloc]initWithText:chat[@"text"] sender:sender.objectId date:(NSDate *)chat[@"date"]];
    
    if ([message.senderId isEqualToString:self.sender]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        PFObject *prevChat = self.chats[indexPath.item - 1];
        //PFUser *prevUser = prevChat[@"fromUser"];
        JSQMessage *previousMessage = [[JSQMessage alloc] initWithSenderId:prevChat[@"senderId"] senderDisplayName:prevChat[@"senderDisplayName"] date:(NSDate *)prevChat[@"date"] text:prevChat[@"text"]];
        if ([previousMessage.senderId isEqualToString:message.senderId]) {
            return nil;
        }
    }
    return [[NSAttributedString alloc] initWithString:message.senderId];
    
}

-(NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

#pragma mark - JSQ DataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    //NSLog(@"%i", (int)[self.chats count]);
    return [self.chats count];
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    PFObject *chat = self.chats[indexPath.item];
    //PFUser *sender = chat[@"fromUser"];
    
    JSQMessage *msg = [[JSQMessage alloc] initWithSenderId:chat[@"senderId"] senderDisplayName:chat[@"senderDisplayName"] date:(NSDate *)chat[@"date"] text:chat[@"text"]];
    //NSLog(@"%@", msg.sender);
    
    if ([msg.senderId isEqualToString:self.sender]) {
        cell.textView.textColor = [UIColor blackColor];
    }
    else {
        cell.textView.textColor = [UIColor whiteColor];
    }
    
    cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                          NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    
    return cell;
}


#pragma mark - JSQ FlowLayout

-(CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    PFObject *chat = self.chats[indexPath.item];
    //PFUser *sender = chat[@"fromUser"];
    JSQMessage *currentMessage = [[JSQMessage alloc] initWithSenderId:chat[@"senderId"] senderDisplayName:chat[@"senderDisplayName"] date:(NSDate *)chat[@"date"] text:chat[@"text"]];
    if ([currentMessage.senderId isEqualToString:self.sender]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        
        PFObject *prevChat = self.chats[indexPath.item - 1];
        //PFUser *prevSender = prevChat[@"fromUser"];
        JSQMessage *previousMessage = [[JSQMessage alloc] initWithSenderId:prevChat[@"senderId"] senderDisplayName:prevChat[@"senderDisplayName"] date:(NSDate *)prevChat[@"date"] text:prevChat[@"text"]];
        
        
        if ([previousMessage.senderId isEqualToString:currentMessage.senderId]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
    //return 0.0f;
}

-(CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

-(CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    return 0.0f;
}


-(void)collectionView:(JSQMessagesCollectionView *)collectionView header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender {
    
    
}

//-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    
//    PFObject *chat = self.chats[indexPath.item];
//    PFUser *sender = chat[@"fromUser"];
//    JSQMessage *message = [[JSQMessage alloc]initWithText:chat[@"text"] sender:sender.objectId date:(NSDate *)chat[@"date"]];
//    
//    
//}


#pragma mark - Helper Methods

-(void) checkForNewChats{
    int oldChatCount = (int)[self.chats count];
    PFQuery *queryForChats = [PFQuery queryWithClassName:@"Chat"];
    [queryForChats whereKey:@"chatRoom" equalTo:self.chatRoom];
//    [queryForChats includeKey:@"fromUser"];
//    [queryForChats includeKey:@"toUser"];
    [queryForChats orderByAscending:@"createdAt"];
    [queryForChats findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if (self.initialLoadComplete == NO || oldChatCount != [objects count]) {
                self.chats = [objects mutableCopy];
                if (self.initialLoadComplete == YES) {
                    [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
                }
                self.initialLoadComplete = YES;
                [self finishReceivingMessage];
                [self.collectionView reloadData];
                [self scrollToBottomAnimated:YES];
            }
        }
    }];
}

//#pragma mark - UIImagePickerControllerDelegate
//
//
//-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    
//    UIImage *image = info[UIImagePickerControllerEditedImage];
//    
//    if (!image) {
//        image = info[UIImagePickerControllerOriginalImage];
//        
//    }
//    self.itemImageView.image = image;
//    
//    [self dismissViewControllerAnimated:YES completion:nil];
//    
//}
//
//-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
//    
//    
//    [self dismissViewControllerAnimated:YES completion:nil];
//    
//}
//
//#pragma mark - pickerView Methods
//
//-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
//    return [self.pickerData count];
//}
//
//-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
//    return 1;
//}
//
//-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    return self.pickerData[row];
//}
//
//-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
//    
//    self.pickerSelection = self.pickerData[row];
//    self.classTypeUpdated = YES;
//    
//}
//
//-(NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    NSAttributedString *attString = [[NSAttributedString alloc]initWithString:self.pickerData[row] attributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:220/255.0f green:175/255.0f blue:90/255.0f alpha:1.0f]}];
//    return attString;
//    
//}

@end
