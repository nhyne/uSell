//
//  AJBuyBookCollectionViewCell.m
//  U
//
//  Created by Adam Johnson on 7/19/14.
//
//

#import "AJBuyBookCollectionViewCell.h"


@implementation AJBuyBookCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {

    self = [super initWithCoder:aDecoder];

    if (self) {
        [self setup];
    }
    return self;

}
-(void) setup {
    
    [self.contentView addSubview:self.itemTitleLabel];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



@end
