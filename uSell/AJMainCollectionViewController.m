//
//  AJMainCollectionViewController.m
//  U
//
//  Created by Adam Johnson on 8/6/14.
//
//

#import "AJMainCollectionViewController.h"
#import "AJMainCollectionViewCell.h"
#import "AJBuyBookViewController.h"
@interface AJMainCollectionViewController () <NSURLConnectionDelegate>

@property (strong, nonatomic) NSString *itemType;
@property (strong, nonatomic) NSMutableData *imageData;


@end

@implementation AJMainCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collectionView.backgroundColor = [AJColors backgroundColor];
    
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // connected, do some internet stuff
        [self buildPfUser];
    }
    
    // Do any additional setup after loading the view.
    //self.collectionView.backgroundColor = [UIColor ]
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AJMainCollectionViewCell *menuCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    menuCell.backgroundColor = [AJColors goldColor];
    menuCell.cellTextLabel.textColor = [AJColors garnetColor];
    
    if (indexPath.item == 0) {
        menuCell.cellImageView.image = [UIImage imageNamed:@"book.jpg"];
        menuCell.cellTextLabel.text = @"Buy Books";
    }
    else if (indexPath.item == 1) {
        menuCell.cellImageView.image = [UIImage imageNamed:@"electronic.jpg"];
        menuCell.cellTextLabel.text = @"Buy Electronics";
    }
    else if (indexPath.item == 2) {
        menuCell.cellImageView.image = [UIImage imageNamed:@"room.jpg"];
        menuCell.cellTextLabel.text = @"Buy Furniture";
    }
    else if (indexPath.item == 3){
        menuCell.cellImageView.image = [UIImage imageNamed:@"other.jpg"];
        menuCell.cellTextLabel.text = @"Buy Everything Else";
    }
    else if (indexPath.item == 4) {
        menuCell.cellImageView.image = [UIImage imageNamed:@"chat.jpeg"];
        menuCell.cellTextLabel.text = @"Your Chats";
    }
    else if (indexPath.item == 5) {
        menuCell.cellImageView.image = [UIImage imageNamed:@"sell.jpg"];
        menuCell.cellTextLabel.text = @"Sell Anything";
    }
    return menuCell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item < 4) {
        
        if (indexPath.item == 0) {
            self.itemType = @"Book";
        }
        else if (indexPath.item == 1) {
            self.itemType = @"Electronic";
        }
        else if (indexPath.item == 2) {
            self.itemType = @"Room";
        }
        else if (indexPath.item == 3) {
            self.itemType = @"Other";
        }
        [self performSegueWithIdentifier:@"mainToBooksSegue" sender:nil];
    }
    else if (indexPath.item == 4) {
        [self performSegueWithIdentifier:@"mainToChatroomsSegue" sender:nil];
    }
    else if (indexPath.item == 5) {
        [self performSegueWithIdentifier:@"mainToSaleSegue" sender:nil];
    }
}



#pragma mark - Actions 

- (IBAction)logoutBarButtonItemPressed:(UIBarButtonItem *)sender {
    
    [PFUser logOut];
    if (FBSession.activeSession.isOpen)
    {
        [FBSession.activeSession closeAndClearTokenInformation];
    }
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    
}

- (IBAction)profileBarButtonItemPressed:(UIBarButtonItem *)sender {
    
    [self performSegueWithIdentifier:@"mainToProfileSegue" sender:sender];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"mainToBooksSegue"] && [segue.destinationViewController isKindOfClass:[AJBuyBookViewController class]]) {
        AJBuyBookViewController *nextView = segue.destinationViewController;
        nextView.itemType = self.itemType;
    }
}

#pragma mark - helper Methods

-(void) buildPfUser {
    
    // Create request for user's Facebook data
    FBRequest *request = [FBRequest requestForMe];
    
    // Send request to Facebook
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // result is a dictionary with the user's Facebook data
            
            NSDictionary *userData = (NSDictionary *)result;
            NSString *facebookID = userData[@"id"];
            NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
            
            
            [[PFUser currentUser]setObject:userData[@"id"] forKey:@"facebookID"];
            [[PFUser currentUser]setObject:userData[@"first_name"] forKey:@"first_name"];
            [[PFUser currentUser]setObject:userData[@"last_name"] forKey:@"last_name"];
            [[PFUser currentUser]setObject:userData[@"gender"] forKey:@"gender"];
            [[PFUser currentUser]setObject:[pictureURL absoluteString] forKey:@"pictureURL"];
            NSMutableArray *tempArray = [[NSMutableArray alloc]init];
            if (![PFUser currentUser][@"starredBooks"]) {
                [[PFUser currentUser]setObject:tempArray forKey:@"starredBooks"];
            }
            if (![PFUser currentUser][@"starredElectronics"]) {
                [[PFUser currentUser]setObject:tempArray forKey:@"starredElectronics"];
            }
            if (![PFUser currentUser][@"starredRooms"]) {
                [[PFUser currentUser]setObject:tempArray forKey:@"starredRooms"];
            }
            if (![PFUser currentUser][@"starredOthers"]) {
                [[PFUser currentUser]setObject:tempArray forKey:@"starredOthers"];
            }
            
            [[PFUser currentUser]saveInBackground];
            [self requestImage];

//            NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:pictureURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2.0f];
//            // Run network request asynchronously
//            NSURLConnection *urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
            
            
        }
    }];
    
}


// Called every time a chunk of the data is received
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.imageData appendData:data]; // Build the image
}

// Called when the entire image is finished downloading
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // Set the image in the header imageView
    UIImage *profileImage = [UIImage imageWithData:self.imageData];
    [self uploadPFFileToParse:profileImage];
}


-(void) uploadPFFileToParse:(UIImage *)image {
    
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
    
    if (!imageData) {
        NSLog(@"Image data was not found");
        return;
    }
    
    PFFile *photoFile = [PFFile fileWithData:imageData];
    [photoFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [[PFUser currentUser] setObject:photoFile forKey:@"picture"];
        }
    }];
    
}


-(void)requestImage {
    
    //[PFUser currentUser][@"picture"];
    
    //PFQuery *query = [PFQuery queryWithClassName:@"User"];
    //[query whereKey:kAJPhotoUserKey equalTo:[PFUser currentUser]];
    NSLog(@"request image");
    //[query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
    //if (number == 0) {
    PFUser *user = [PFUser currentUser];
    self.imageData = [[NSMutableData alloc]init];
    
    NSURL *profilePictureURL = [NSURL URLWithString:user[@"pictureURL"]];
    NSLog(@"now loading picture");
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:profilePictureURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0f];
    NSURLConnection *urlConnection = [[NSURLConnection alloc]initWithRequest:urlRequest delegate:self];
    NSLog(@"picutre loaded");
    if (!urlConnection) {
        NSLog(@"Failed to download picture");
    }
    //}
    //}];
    
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

//if (![self connected])
//{
//    // not connected
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//    [alert show];
//}
//else
//{
//    // connected, do some internet stuff
//    
//}


@end
