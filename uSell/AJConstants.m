//
//  AJConstants.m
//  U
//
//  Created by Adam Johnson on 7/10/14.
//
//

#import "AJConstants.h"

@implementation AJConstants

#pragma mark - Book Class

NSString *const kAJBookClassKey = @"Book";
NSString *const kAJBookSellerKey = @"seller";
NSString *const kAJBookVisibleKey = @"visible";
NSString *const kAJBookTitleKey = @"title";
NSString *const kAJBookAuthorKey = @"author";
NSString *const kAJBookPictureKey = @"picture";
NSString *const kAJBookConditionKey = @"condition";
NSString *const kAJBookCostKey = @"cost";
NSString *const kAJBookCourseKey = @"course";
NSString *const kAJBookEditionKey = @"editionNumber";


#pragma mark - Item Class Numbers

int *const kAJBookClassNumberKey = 0;
int *const kAJElectronicClassNumberKey = 1;
int *const kAJRoomClassNumberKey = 2;
int *const kAJOtherClassNumberKey = 3;


#pragma mark - User Class

NSString *const kAJUserClassKey = @"User";

NSString *const kAJUserProfileKey = @"profile";
NSString *const kAJUserProfileNameKey = @"name";
NSString *const kAJUserProfileFirstNameKey = @"firstName";
NSString *const kAJUserProfileLocationKey = @"location";
NSString *const kAJUserProfileGenderKey = @"gender";
NSString *const kAJUserProfileBirthdayKey = @"birthday";
NSString *const kAJUserProfileInterestedInKey = @"interestedIn";
NSString *const kAJUserProfilePictureURL = @"pictureURL";
NSString *const kAJUserProfileRelationshipStatusKey = @"relationshipStatus";
NSString *const kAJUserProfileAgeKey = @"age";


@end
