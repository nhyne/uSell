//
//  AJChatViewController.h
//  U
//
//  Created by Adam Johnson on 7/24/14.
//
//

#import "JSQMessagesViewController.h"
#import "JSQMessages.h"


@interface AJChatViewController : JSQMessagesViewController <JSQMessagesCollectionViewDataSource, JSQMessagesCollectionViewDelegateFlowLayout>

@property (strong, nonatomic) PFObject *chatRoom;

@end
