//
//  AJColors.m
//  USell
//
//  Created by Adam Johnson on 9/1/14.
//
//

#import "AJColors.h"
#import <UIKit/UIKit.h>

@interface AJColors ()

@end

@implementation AJColors

+(UIColor *) backgroundColor {
    return [UIColor colorWithRed:80/255.0f green:10/255.0f blue:10/255.0f alpha:1.0f];
    
}


+(UIColor *) garnetColor {
   return [UIColor colorWithRed:99/255.0f green:20/255.0f blue:20/255.0f alpha:1.0f];
    
}


+(UIColor *) goldColor {
    
    return [UIColor colorWithRed:220/255.0f green:175/255.0f blue:90/255.0f alpha:1.0f];
}


@end
