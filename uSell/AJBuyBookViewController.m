//
//  AJBuyBookViewController.m
//  U
//
//  Created by Adam Johnson on 7/18/14.
//
//

#import "AJBuyBookViewController.h"
#import "AJBuyBookCollectionViewCell.h"
#import "AJBookInfoViewController.h"

@interface AJBuyBookViewController ()

@property (strong, nonatomic) NSMutableArray *items;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivityIndicator;

@end

@implementation AJBuyBookViewController

-(NSMutableArray *) items{
    
    if (!_items) {
        _items = [[NSMutableArray alloc]init];
    }
    return _items;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.collectionView.backgroundColor = [AJColors backgroundColor];
    self.loadingActivityIndicator.color = [AJColors goldColor];
    self.loadingActivityIndicator.hidden = YES;
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // connected, do some internet stuff
        self.loadingActivityIndicator.hidden = NO;
        [self.loadingActivityIndicator startAnimating];
        PFQuery *queryForBooks = [PFQuery queryWithClassName:self.itemType];
        //[queryForBooks whereKey:@"seller" notEqualTo:[PFUser currentUser]];
        [queryForBooks whereKey:@"visible" equalTo:@1];
        [queryForBooks whereKey:@"sellerSchool" containedIn:[PFUser currentUser][@"savedSchools"]];
        //[queryForBooks orderByAscending:@"createdAt"];
        [queryForBooks findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            self.items = [objects mutableCopy];
            self.loadingActivityIndicator.hidden = YES;
            [self.loadingActivityIndicator stopAnimating];
            [self.collectionView reloadData];
        }];
    }
    
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"buyToInfoSegue"] && [segue.destinationViewController isKindOfClass:[AJBookInfoViewController class]]) {
        AJBookInfoViewController *nextView = segue.destinationViewController;
        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems]lastObject]; //last selected item
        PFObject *selectedBookObject = self.items[indexPath.item];
        nextView.item= selectedBookObject;
    }
    
    
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - Collection View Methods

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    AJBuyBookCollectionViewCell *bookCell =[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    bookCell.itemActivityIndacator.hidden = NO;
    [bookCell.itemActivityIndacator startAnimating];
    bookCell.itemActivityIndacator.color = [AJColors goldColor];
    bookCell.itemTitleLabel.hidden = YES;
    
    PFFile *file = self.items[indexPath.item][@"picture"];
    [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error) {
            UIImage *image = [UIImage imageWithData:data];
            [bookCell.itemActivityIndacator stopAnimating];
            bookCell.itemActivityIndacator.hidden = YES;
            bookCell.itemTitleLabel.hidden = NO;
            bookCell.itemImageView.image = image;
            bookCell.backgroundColor = [AJColors goldColor];
            bookCell.itemTitleLabel.textColor = [AJColors goldColor];
            bookCell.itemTitleLabel.text = self.items[indexPath.item][@"title"];
        }
        else NSLog(@"%@", error);
    }];
    
    

    
    
    return bookCell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"buyToInfoSegue" sender:nil];
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.items count];
}
- (IBAction)plusBarButtonItemPressed:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"booksToProfileSegue" sender:sender];
    
}

#pragma mark - Helper Methods

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}


@end
