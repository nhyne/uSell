//
//  AJChatroomsViewController.m
//  USell
//
//  Created by Adam Johnson on 9/6/14.
//
//

#import "AJChatroomsViewController.h"
#import "AJChatViewController.h"
#import "AJEditTableViewCell.h"

@interface AJChatroomsViewController ()

@property (strong, nonatomic) NSMutableArray *chatrooms;
@property (strong, nonatomic) NSMutableArray *otherUsers;
@property (strong, nonatomic) NSMutableArray *imageArray;
@property (strong, nonatomic) NSMutableArray *nameArray;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) PFObject *tempChatroom;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation AJChatroomsViewController

-(NSMutableArray *) chatrooms {
    if (!_chatrooms) {
        _chatrooms = [[NSMutableArray alloc]init];
    }
    return _chatrooms;
}
-(NSMutableArray *) otherUsers {
    if (!_otherUsers) {
        _otherUsers = [[NSMutableArray alloc]init];
    }
    return _otherUsers;
}
-(NSMutableArray *) imageArray {
    if (!_imageArray) {
        _imageArray = [[NSMutableArray alloc]init];
    }
    return _imageArray;
}
-(NSMutableArray *) nameArray {
    if (!_nameArray) {
        _nameArray = [[NSMutableArray alloc]init];
    }
    return _nameArray;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [AJColors backgroundColor];
    self.tableView.backgroundColor = [AJColors backgroundColor];
    [self.tableView setSeparatorColor:[AJColors goldColor]];
    self.activityIndicator.color = [AJColors goldColor];
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // connected, do some internet stuff
        self.tableView.hidden = YES;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.activityIndicator.hidden = NO;
        [self.activityIndicator startAnimating];
        [self getChatrooms];
    }
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.imageArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AJEditTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.cellTextLabel.textColor = [AJColors goldColor];
    cell.cellDetailLabel.textColor = [AJColors goldColor];
    cell.backgroundColor = [AJColors garnetColor];
    
    
    // cell.textLabel.text = self.otherUsers[indexPath.row];
    
    
    PFQuery *queryForChat = [PFQuery queryWithClassName:@"Chat"];
    [queryForChat whereKey:@"chatRoom" equalTo:self.chatrooms[indexPath.row]];
    [queryForChat orderByDescending:@"date"];
    [queryForChat getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        cell.cellDetailLabel.text = object[@"text"];
    }];
    
    cell.cellItemImageView.image = self.imageArray[indexPath.row];
    cell.cellTextLabel.text = self.nameArray[indexPath.row];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tempChatroom = self.chatrooms[indexPath.row];
    [self performSegueWithIdentifier:@"chatroomsToChatSegue" sender:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[AJChatViewController class]] && [segue.identifier isEqualToString:@"chatroomsToChatSegue"]) {
        AJChatViewController *nextView = segue.destinationViewController;
        nextView.chatRoom = self.tempChatroom;
        self.tempChatroom = nil;
    }
}


#pragma mark - Helper Methods

-(void) getChatrooms {
    
    PFQuery *queryForChatrooms = [PFQuery queryWithClassName:@"ChatRoom"];
    [queryForChatrooms whereKey:@"user1" equalTo:[PFUser currentUser]];
    PFQuery *queryForChatroomsInverse = [PFQuery queryWithClassName:@"ChatRoom"];
    [queryForChatroomsInverse whereKey:@"user2" equalTo:[PFUser currentUser]];
    
    PFQuery *combinedQuery = [PFQuery orQueryWithSubqueries:@[queryForChatroomsInverse, queryForChatrooms]];
    [combinedQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            self.chatrooms = [objects mutableCopy];
            NSLog(@"%@", objects);
            for (PFObject *chatRoom in objects) {
                PFUser *user1 = chatRoom[@"user1"];
                PFUser *user2 = chatRoom[@"user2"];
                if ([[PFUser currentUser] isEqual:user1]) {
                    [self.otherUsers addObject:user2];
                }
                else {
                    [self.otherUsers addObject:user1];
                }
            }
            [self buildImageTextArrays];
            [self.tableView reloadData];
            self.activityIndicator.hidden = YES;
            [self.activityIndicator stopAnimating];
            self.tableView.hidden = NO;
        }
    }];
    
    
}

-(PFUser *) getOtherUser :(NSIndexPath *) indexPath {
    
    PFUser *user1 = self.chatrooms[indexPath.row][@"user1"];
    PFUser *user2 = self.chatrooms[indexPath.row][@"user2"];
    if ([[PFUser currentUser] isEqual:user1]) {
        return user2;
    }
    else {
        return user1;
    }
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}


-(void) buildImageTextArrays {
    
    for (PFUser *otherUser in self.otherUsers) {
        [otherUser fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            [self.nameArray addObject:[NSString stringWithFormat:@"%@ %@", otherUser[@"first_name"], otherUser[@"last_name"]]];
            //cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", otherUser[@"first_name"], otherUser[@"last_name"]];
            PFFile *imageFile = otherUser[@"picture"];
            [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                if (!error) {
                    UIImage *image = [UIImage imageWithData:data];
                    [self.imageArray addObject:image];
                    [self.tableView reloadData];
                    NSLog(@"reload data");
                    //cell.imageView.image = image;
                }
                else {
                    NSLog(@"%@", error);
                }
            }];
        }];
        //[self.tableView reloadData];
    }
    
    
}



@end
