//
//  AJSchoolsTableViewController.m
//  USell
//
//  Created by Adam Johnson on 8/15/14.
//
//

#import "AJSchoolsTableViewController.h"
#import "AJSchoolsTableViewCell.h"
#import "AJStateAndSchoolData.h"

@interface AJSchoolsTableViewController () <AJSavedSchoolsCellProtocal>
@property (strong, nonatomic) NSDictionary *schoolsDictionary;
@property (strong, nonatomic) NSMutableArray *unsavedSchools;
@end

@implementation AJSchoolsTableViewController

-(NSMutableArray *) savedSchoolsArray {
    if (!_savedSchoolsArray) {
        _savedSchoolsArray = [[NSMutableArray alloc]init];
    }
    return _savedSchoolsArray;
}

-(NSMutableArray *) unsavedSchools {
    if (!_unsavedSchools) {
        _unsavedSchools = [[NSMutableArray alloc]init];
    }
    return _unsavedSchools;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self buildDictionary];
    NSArray *saves = [self.savedSchoolsArray copy];
    self.unsavedSchools = [self.schoolsDictionary[self.state] mutableCopy];
    [self.unsavedSchools removeObjectsInArray:saves];
    self.tableView.backgroundColor = [AJColors backgroundColor];
    [self.tableView setSeparatorColor:[AJColors goldColor]];
    
    //self.savedSchoolsArray = [[[PFUser currentUser]objectForKey:@"savedSchools"] mutableCopy];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
-(void) viewWillDisappear:(BOOL)animated {
    
    [self.delegate updateSavedSchools:self.savedSchoolsArray];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.unsavedSchools count];
    //return [self.schoolsDictionary[self.state] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AJSchoolsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSString *schoolName = self.unsavedSchools[indexPath.row];
    cell.backgroundColor = [AJColors garnetColor];
    cell.schoolNameLabel.textColor = [AJColors goldColor];
    cell.schoolNameLabel.text = schoolName;

    cell.delegate = self;
    return cell;
    
}


#pragma mark - AJSavedSchoolsCellProtocal Methods

-(void) saveSchool:(NSString *)schoolToSave {
    [self.savedSchoolsArray addObject: [NSString stringWithFormat:@"%@, %@", schoolToSave, self.stateAbbreviation]];
    [self.unsavedSchools removeObject:schoolToSave];
    [self.tableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Helper Methods

-(void) buildDictionary {
    
    self.schoolsDictionary = [AJStateAndSchoolData stateNamesToSchoolsDictionary];
    
}


@end
