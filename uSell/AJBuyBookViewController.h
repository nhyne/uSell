//
//  AJBuyBookViewController.h
//  U
//
//  Created by Adam Johnson on 7/18/14.
//
//

#import <UIKit/UIKit.h>

@interface AJBuyBookViewController : UICollectionViewController

@property (strong, nonatomic) NSString *itemType;

@end
