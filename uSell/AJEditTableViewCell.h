//
//  AJEditTableViewCell.h
//  USell
//
//  Created by Adam Johnson on 8/24/14.
//
//

#import <UIKit/UIKit.h>

@interface AJEditTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellItemImageView;
@property (weak, nonatomic) IBOutlet UILabel *cellTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *cellDetailLabel;

@end
