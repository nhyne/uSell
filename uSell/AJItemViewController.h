//
//  AJItemViewController.h
//  USell
//
//  Created by Adam Johnson on 8/10/14.
//
//

#import <UIKit/UIKit.h>

@protocol AJItemSaleProtocal <NSObject>

-(void) updateView:(PFObject *)itemToAdd :(NSString *) classToAddTo;

@end


@interface AJItemViewController : UIViewController

@property (weak, nonatomic) id <AJItemSaleProtocal> delegate;
@property (strong, nonatomic) PFObject *item;

@end
