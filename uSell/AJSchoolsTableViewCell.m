//
//  AJSchoolsTableViewCell.m
//  USell
//
//  Created by Adam Johnson on 8/16/14.
//
//

#import "AJSchoolsTableViewCell.h"

@interface AJSchoolsTableViewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *plusMinusButtonImageView;
@property (strong, nonatomic) IBOutlet UIButton *plusMinusButton;



@end


@implementation AJSchoolsTableViewCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)plusMinusButtonPressed:(UIButton *)sender {
    NSString *schoolNameString = self.schoolNameLabel.text;
    [self.delegate saveSchool:schoolNameString];
    
    
}

-(void) setup {
    self.plusMinusButtonImageView.image = [UIImage imageNamed:@"plus-50.png"];
    
}


@end
