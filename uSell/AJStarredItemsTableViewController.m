//
//  AJStarredItemsTableViewController.m
//  USell
//
//  Created by Adam Johnson on 8/21/14.
//
//

#import "AJStarredItemsTableViewController.h"
#import "AJBookInfoViewController.h"
#import "AJEditTableViewCell.h"
@interface AJStarredItemsTableViewController ()

@property (strong, nonatomic) NSMutableArray *starredBooks;
@property (strong, nonatomic) NSMutableArray *starredElectronics;
@property (strong, nonatomic) NSMutableArray *starredRooms;
@property (strong, nonatomic) NSMutableArray *starredOthers;

@property (strong, nonatomic) PFObject *tempObject;

@property (nonatomic) BOOL didDeleteItem;

@end

@implementation AJStarredItemsTableViewController

-(NSMutableArray *) starredBooks {
    if (!_starredBooks) {
        _starredBooks = [[NSMutableArray alloc]init];
    }
    return _starredBooks;
}
-(NSMutableArray *) starredElectronics {
    if (!_starredElectronics) {
        _starredElectronics = [[NSMutableArray alloc]init];
    }
    return _starredElectronics;
}
-(NSMutableArray *) starredRooms {
    if (!_starredRooms) {
        _starredRooms = [[NSMutableArray alloc]init];
    }
    return _starredRooms;
}
-(NSMutableArray *) starredOthers {
    if (!_starredOthers) {
        _starredOthers = [[NSMutableArray alloc]init];
    }
    return _starredOthers;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self handleColors];
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // connected, do some internet stuff
        [self handleArrayCreation];
        NSLog(@"%@", self.starredRooms);
        
    }
    
    
    self.didDeleteItem = NO;
    
}

-(void)viewWillDisappear:(BOOL)animated {
    
    
    if (self.didDeleteItem) {
        [[PFUser currentUser] setObject:self.starredBooks forKey:@"starredBooks"];
        [[PFUser currentUser]setObject:self.starredElectronics forKey:@"starredElectronics"];
        [[PFUser currentUser]setObject:self.starredRooms forKey:@"starredRooms"];
        [[PFUser currentUser] setObject:self.starredOthers forKey:@"starredOthers"];
        [[PFUser currentUser]saveInBackground];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [self.starredBooks count];
    }
    else if (section == 1) {
        return [self.starredElectronics count];
    }
    else if (section == 2) {
        return [self.starredRooms count];
    }
    else if (section == 3) {
        return [self.starredOthers count];
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AJEditTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.cellTextLabel.textColor = [AJColors goldColor];
    cell.backgroundColor = [AJColors garnetColor];
    NSMutableArray *sectionArray = [self getSectionArray:indexPath];
    cell.cellTextLabel.text = sectionArray[indexPath.row][@"title"];
    cell.cellDetailLabel.textColor = [AJColors goldColor];
    cell.cellDetailLabel.text = [NSString stringWithFormat:@"%@", sectionArray[indexPath.row][@"cost"]];
    PFFile *imageFile = sectionArray[indexPath.row][@"picture"];
    [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error) {
            UIImage *image = [UIImage imageWithData:data];
            cell.cellItemImageView.image = image;
            //[self.tableView reloadData];
        }
    }];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray *sectionArray = [self getSectionArray:indexPath];
    self.tempObject = sectionArray[indexPath.row];
    [self performSegueWithIdentifier:@"starredItemsToItemInfoSegue" sender:nil];
    
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        self.didDeleteItem = YES;
        if (indexPath.section == 0) {
            [self.starredBooks removeObjectAtIndex:indexPath.row];
        }
        else if (indexPath.section == 1) {
            [self.starredElectronics removeObjectAtIndex:indexPath.row];
        }
        else if (indexPath.section == 2) {
            [self.starredRooms removeObjectAtIndex:indexPath.row];
        }
        else if (indexPath.section == 3) {
            [self.starredOthers removeObjectAtIndex:indexPath.row];
        }
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[AJBookInfoViewController class]] & [segue.identifier isEqualToString:@"starredItemsToItemInfoSegue"]) {
        AJBookInfoViewController *nextView = segue.destinationViewController;
        nextView.item = self.tempObject;
        self.tempObject = nil;
    }
}



#pragma mark - Helper Methods

-(NSMutableArray *) getSectionArray :(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return self.starredBooks;
    }
    else if (indexPath.section == 1) {
        return self.starredElectronics;
    }
    else if (indexPath.section == 2) {
        return self.starredRooms;
    }
    else if (indexPath.section == 3) {
        return self.starredOthers;
    }
    else {
        return nil;
    }
    
}

-(void) handleArrayCreation {
    PFQuery *queryForStarredBooks = [PFQuery queryWithClassName:@"Book"];
    [queryForStarredBooks whereKey:@"objectId" containedIn:[PFUser currentUser][@"starredBooks"]];
    [queryForStarredBooks whereKey:@"visible" equalTo:@1];
    [queryForStarredBooks findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            self.starredBooks = [objects mutableCopy];
            [self.tableView reloadData];
        }
    }];
    
    PFQuery *queryForStarredElectronics = [PFQuery queryWithClassName:@"Electronic"];
    [queryForStarredElectronics whereKey:@"objectId" containedIn:[PFUser currentUser][@"starredElectronics"]];
    [queryForStarredElectronics whereKey:@"visible" equalTo:@1];
    [queryForStarredElectronics findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            self.starredElectronics = [objects mutableCopy];
            [self.tableView reloadData];
        }
    }];
    
    PFQuery *queryForStarredRooms = [PFQuery queryWithClassName:@"Room"];
    [queryForStarredRooms whereKey:@"objectId" containedIn:[PFUser currentUser][@"starredRooms"]];
    [queryForStarredRooms whereKey:@"visible" equalTo:@1];
    [queryForStarredRooms findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            self.starredRooms = [objects mutableCopy];
            [self.tableView reloadData];
        }
    }];
    
    PFQuery *queryForStarredOthers = [PFQuery queryWithClassName:@"Other"];
    [queryForStarredOthers whereKey:@"objectId" containedIn:[PFUser currentUser][@"starredOthers"]];
    [queryForStarredOthers whereKey:@"visible" equalTo:@1];
    [queryForStarredOthers findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            self.starredOthers = [objects mutableCopy];
            [self.tableView reloadData];
        }
    }];
}


- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}


-(void) handleColors {
    
    self.tableView.backgroundColor = [AJColors backgroundColor];
    [self.tableView setSeparatorColor:[AJColors goldColor]];
    
}





@end
