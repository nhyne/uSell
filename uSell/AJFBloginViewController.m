//
//  AJFBloginViewController.m
//  uSell
//
//  Created by Adam Johnson on 1/2/15.
//  Copyright (c) 2015 Adam Johnson. All rights reserved.
//

#import "AJFBloginViewController.h"

@interface AJFBloginViewController () <FBLoginViewDelegate>

@property (strong, nonatomic) IBOutlet FBProfilePictureView *profilePictureView;
@property (strong, nonatomic) FBLoginView *loginView;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSArray *permissionsArray;

@end

@implementation AJFBloginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loginViewButton];
    self.view.backgroundColor = [AJColors backgroundColor];
    self.nameLabel.textColor = [AJColors goldColor];
    self.statusLabel.textColor = [AJColors goldColor];
    self.activityIndicator.color = [AJColors goldColor];
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
    
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // connected, do some internet stuff
        
    }
    
    
    
    
    
    
    
    // Do any additional setup after loading the view.
    
    
    
    // Align the button in the center horizontally
    
    //    [self.view addSubview:self.loginView];
    //    self.permissionsArray = @[@"public_profile", @"email"];
    //    self.loginView.readPermissions = self.permissionsArray;
    //
    //
    //    [self.view addSubview:self.loginView];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.activityIndicator.hidden = YES;
    [self.activityIndicator startAnimating];
    
}

-(void) viewDidAppear:(BOOL)animated {
    
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok"otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // connected, do some internet stuff
        self.loginView.delegate = self;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - FBLoginViewDelegate Methods

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user {
    
    self.profilePictureView.profileID = user.id;
    self.nameLabel.text = user.name;
}

-(void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    
    self.statusLabel.text = @"You're logged in as";
    
    [self.activityIndicator startAnimating];
    self.activityIndicator.hidden = NO;
    [self locatePFUser];
    
}

-(void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    
    self.profilePictureView.profileID = nil;
    self.nameLabel.text = @"";
    self.statusLabel.text= @"You're not logged in!";
    
}

// Handle possible errors that can occur during login
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures that happen outside of the app
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}


#pragma mark - Helper Methods

-(void)locatePFUser {
    
    NSArray *permissionsArray = self.permissionsArray;
    
    // Login PFUser using Facebook
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        [self.activityIndicator stopAnimating]; // Hide loading indicator
        
        if (!user) {
            if (!error) {
                NSLog(@"The user cancelled the Facebook login.");
            } else {
                //NSLog(@"An error occurred: %@", error);
            }
        } else if (user.isNew) {
            NSLog(@"User with facebook signed up and logged in!");
            [self performSegueWithIdentifier:@"loginToMainSegue" sender:nil];
            
        } else {
            NSLog(@"User with facebook logged in!");
            [self performSegueWithIdentifier:@"loginToMainSegue" sender:nil];
            self.activityIndicator.hidden = YES;
            
        }
    }];
    
    
    
}

- (BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

-(void) loginViewButton {
    
    self.loginView = [[FBLoginView alloc] init];
    self.loginView.frame = CGRectOffset(self.loginView.frame, (self.view.center.x - (self.loginView.frame.size.width / 2)), 400);
    //self.loginView.frame = CGRectOffset(self.loginView.frame, (self.view.center.x - (self.loginView.frame.size.width / 2)), 0);
    [self.view addSubview:self.loginView];
    
}


@end
