//
//  AJBookInfoViewController.h
//  U
//
//  Created by Adam Johnson on 7/18/14.
//
//

#import <UIKit/UIKit.h>

@interface AJBookInfoViewController : UIViewController

@property (strong, nonatomic) PFObject *item;

@end
