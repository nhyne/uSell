//
//  AJEditViewController.m
//  USell
//
//  Created by Adam Johnson on 9/1/14.
//
//

#import "AJEditViewController.h"
#import "AJEditTableViewCell.h"

@interface AJEditViewController ()
@property (strong, nonatomic) NSMutableArray *books;
@property (strong, nonatomic) NSMutableArray *electronics;
@property (strong, nonatomic) NSMutableArray *rooms;
@property (strong, nonatomic) NSMutableArray *others;


@property (strong, nonatomic) PFObject *tempItem;
@property (nonatomic) NSInteger itemPulledFrom;
@end

@implementation AJEditViewController

-(NSMutableArray *)books {
    
    if (!_books) {
        _books = [[NSMutableArray alloc]init];
    }
    return _books;
}
-(NSMutableArray *)electronics {
    
    if (!_electronics) {
        _electronics = [[NSMutableArray alloc]init];
    }
    return _electronics;
}
-(NSMutableArray *)rooms {
    
    if (!_rooms) {
        _rooms = [[NSMutableArray alloc]init];
    }
    return _rooms;
}
-(NSMutableArray *)others {
    
    if (!_others) {
        _others = [[NSMutableArray alloc]init];
    }
    return _others;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    else
    {
        // connected, do some internet stuff
        [self handleQueries];
        
    }
    
    
    
    self.tableView.backgroundColor = [AJColors backgroundColor];
    [self.tableView setSeparatorColor:[AJColors goldColor]];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewDidAppear:(BOOL)animated {
    
    //[self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    if (section == 0) {
        return [self.books count];
    }
    else if (section == 1) {
        return [self.electronics count];
    }
    else if (section == 2) {
        return [self.rooms count];
    }
    else if (section == 3) {
        return [self.others count];
    }
    else {
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AJEditTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.backgroundColor = [AJColors garnetColor];
    NSMutableArray *sectionArray = [self getSectionArray:indexPath];
    if (!cell.imageView.image) {
        PFFile *imageFile = sectionArray[indexPath.row][kAJBookPictureKey];
        [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *image = [UIImage imageWithData:data];
                cell.cellItemImageView.image = image;
            }
            else {
                NSLog(@"%@", error);
            }
        }];
    }
    cell.cellTextLabel.textColor = [AJColors goldColor];
    cell.cellDetailLabel.textColor = [AJColors goldColor];
    cell.cellTextLabel.text = sectionArray[indexPath.row][kAJBookTitleKey];
    cell.cellDetailLabel.text = [NSString stringWithFormat:@"%@", sectionArray[indexPath.row][@"cost"]];
    
    return cell;
    
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        
        if (indexPath.section == 0) {
            [self deleteFromParse:self.books[indexPath.row]];
            [self.books removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            //[self.tableView reloadData];
            //[self deleteFromParse:self.books[indexPath.row] :indexPath];
        }
        else if (indexPath.section == 1) {
            [self deleteFromParse:self.electronics[indexPath.row]];
            [self.electronics removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            //[self.tableView reloadData];
        }
        else if (indexPath.section == 2) {
            [self deleteFromParse:self.rooms[indexPath.row]];
            [self.rooms removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            //[self.tableView reloadData];
        }
        else if (indexPath.section == 3) {
            [self deleteFromParse:self.others[indexPath.row]];
            [self.others removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            //[self.tableView reloadData];
        }
        
        
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.destinationViewController isKindOfClass:[AJItemViewController class]] && [segue.identifier isEqualToString:@"editToUpdateItemSegue"]) {
        AJItemViewController *nextView = segue.destinationViewController;
        nextView.item = self.tempItem;
        nextView.delegate = self;
    }
    
    else if ([segue.destinationViewController isKindOfClass:[AJItemViewController class]] && [segue.identifier isEqualToString:@"editToItemSaleSegue"]) {
        
        AJItemViewController *nextView = segue.destinationViewController;
        nextView.delegate = self;
        
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (![self connected])
    {
        // not connected
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Not Found"message:@"Please check your network settings!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        // connected, do some internet stuff
        if (indexPath.section == 0) {
            self.tempItem = self.books[indexPath.row];
            [self.books removeObjectAtIndex:indexPath.row];
            self.itemPulledFrom = 0;
        }
        else if (indexPath.section == 1) {
            self.tempItem = self.electronics[indexPath.row];
            [self.electronics removeObjectAtIndex:indexPath.row];
            self.itemPulledFrom = 1;
        }
        else if (indexPath.section == 2) {
            self.tempItem = self.rooms[indexPath.row];
            [self.rooms removeObjectAtIndex:indexPath.row];
            self.itemPulledFrom = 2;
        }
        else {
            self.tempItem = self.others[indexPath.row];
            [self.others removeObjectAtIndex:indexPath.row];
            self.itemPulledFrom = 3;
        }
        [self performSegueWithIdentifier:@"editToUpdateItemSegue" sender:nil];
    }
    
    
    
}

#pragma mark - SellItemProtocal

-(void) updateView:(PFObject *)itemToAdd :(NSString *)classToAddTo {
    if ([classToAddTo isEqualToString:@"Book"]) {
        [self.books addObject:itemToAdd];
    }
    else if ([classToAddTo isEqualToString:@"Electronic"]) {
        [self.electronics addObject:itemToAdd];
    }
    else if ([classToAddTo isEqualToString:@"Room"]) {
        [self.rooms addObject:itemToAdd];
    }
    else {
        [self.others addObject:itemToAdd];
    }
    self.tempItem = nil;
    [self.tableView reloadData];
}


- (IBAction)plusBarButtonItemPressed:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"editToItemSaleSegue" sender:nil];
}

#pragma mark - Helper Methods


-(NSMutableArray *) getSectionArray :(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return self.books;
    }
    else if (indexPath.section == 1) {
        return self.electronics;
    }
    else if (indexPath.section == 2) {
        return self.rooms;
    }
    else if (indexPath.section == 3) {
        return self.others;
    }
    else {
        return nil;
    }
}

-(void) handleQueries {
    PFQuery *queryForBookss = [PFQuery queryWithClassName:@"Book"];
    [queryForBookss whereKey:@"seller" equalTo:[PFUser currentUser]];
    [queryForBookss whereKey:kAJBookVisibleKey equalTo:@1];
    
    PFQuery *queryForElectronics = [PFQuery queryWithClassName:@"Electronic"];
    [queryForElectronics whereKey:@"seller" equalTo:[PFUser currentUser]];
    [queryForElectronics whereKey:kAJBookVisibleKey equalTo:@1];
    
    PFQuery *queryForRooms = [PFQuery queryWithClassName:@"Room"];
    [queryForRooms whereKey:@"seller" equalTo:[PFUser currentUser]];
    [queryForRooms whereKey:kAJBookVisibleKey equalTo:@1];
    
    PFQuery *queryForOthers = [PFQuery queryWithClassName:@"Other"];
    [queryForOthers whereKey:@"seller" equalTo:[PFUser currentUser]];
    [queryForOthers whereKey:kAJBookVisibleKey equalTo:@1];
    
    [queryForBookss findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        self.books = [objects mutableCopy];
        [self.tableView reloadData];
    }];
    
    [queryForElectronics findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        self.electronics = [objects mutableCopy];
        [self.tableView reloadData];
    }];
    
    [queryForRooms findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        self.rooms = [objects mutableCopy];
        [self.tableView reloadData];
    }];
    
    [queryForOthers findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        self.others = [objects mutableCopy];
        [self.tableView reloadData];
    }];
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

-(void) deleteFromParse :(PFObject *)objectToDelete {
    PFQuery *queryForItemToDelete = [PFQuery queryWithClassName:[objectToDelete parseClassName]];
    [queryForItemToDelete getObjectInBackgroundWithId:[objectToDelete objectId] block:^(PFObject *object, NSError *error) {
        if (!error) {
            [object setValue:@0 forKeyPath:@"visible"];
            [object saveInBackground];
        }
        else {
            NSLog(@"%@", error);
        }
    }];
}


@end
