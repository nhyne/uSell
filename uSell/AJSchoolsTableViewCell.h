//
//  AJSchoolsTableViewCell.h
//  USell
//
//  Created by Adam Johnson on 8/16/14.
//
//

#import <UIKit/UIKit.h>

@protocol AJSavedSchoolsCellProtocal <NSObject>

-(void) saveSchool :(NSString *) schoolToSave;

@end


@interface AJSchoolsTableViewCell : UITableViewCell

@property (weak, nonatomic) id <AJSavedSchoolsCellProtocal> delegate;
//@property (nonatomic) BOOL schoolAdded;
@property (weak, nonatomic) IBOutlet UILabel *schoolNameLabel;

@end
